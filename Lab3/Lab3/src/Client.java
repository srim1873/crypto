import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Client {
	
	public PrintStream out2;
	
	private HttpsURLConnection getHttpsClient(String url) throws Exception {
        // Security section START
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
 
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
 
                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};
 
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Security section END
        
        HttpsURLConnection client = (HttpsURLConnection) new URL(url).openConnection();
       
        
        //add request header
       client.setRequestProperty("User-Agent",
              "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
       
       return client;
    }
 
    private void getHTTP() throws Exception {
        
        String url = "https://bnr.ro/Home.aspx";
        HttpsURLConnection client = getHttpsClient(url);
 
        int responseCode = client.getResponseCode();
        System.out.println("GET request to URL: " + url);
        System.out.println("Response Code     : " + responseCode);
        
        try (BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
 
            StringBuilder response = new StringBuilder();
            String line;
 
            while ((line = in.readLine()) != null) {
                response.append(line).append("\n");
            }
            
            try (PrintStream out = new PrintStream(new FileOutputStream("http_bnr.txt"))) {
                out.print(response.toString());
            }
        }
    }
    
  
	private void getCertificateInformation(HttpsURLConnection client) throws IOException, CertificateParsingException, CertificateEncodingException {
		client.connect();
		System.out.println();
		System.out.println("***TANUSITVANY***");
		Certificate[] certs = client.getServerCertificates();
		out2.append(certs[0].toString());
		
		X509Certificate cert = (X509Certificate) certs[0];
		System.out.println("Verzioszam: "+cert.getVersion());
		System.out.println("Szeriaszam: "+cert.getSerialNumber());
		System.out.println("A tanusito hatosag neve: "+ cert.getIssuerDN().getName());
		System.out.println("Kibocsatas datuma: "+cert.getNotBefore().toString());
		System.out.println("Ervenyessegi ido: "+cert.getNotBefore().toString()+ " -- "+ cert.getNotAfter().toString());
		System.out.println("Tanusitvany alanyanak adatai:\n"
				+"    Neve: "+cert.getSubjectDN().getName()+ "\n"
				+"    Internetes cimei: "+cert.getSubjectAlternativeNames().toString());
		System.out.println("Nyilvanos kulcs adatai:\n"
				+"    Titkosito eljaras tipusa: "+cert.getPublicKey().getAlgorithm()+"\n"  //nem biztos
				+"    Az alany nyilvanos kulcsa: "+cert.getPublicKey());
		
	}
	
	 public static void main(String[] args) throws Exception {
		 	
	        Client obj = new Client();
	        obj.out2 = new PrintStream(new FileOutputStream("certificate.txt"));
	        obj.getHTTP();
	        HttpsURLConnection client = obj.getHttpsClient("https://bnr.ro/Home.aspx");
	        obj.getCertificateInformation(client);
	    }

}
