
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.security.cert.Certificate;

public class Client2 {

	private SSLSocket socket;

    public Client2() {
        try {
            SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            socket = (SSLSocket) factory.createSocket("bnr.ro", 443);
            socket.startHandshake();
            Certificate[] certificates = socket.getSession().getPeerCertificates();
            System.out.println(certificates[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveContentToFile() {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
            out.println("GET /Home.aspx HTTP/1.0");
            out.println();
            out.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            FileWriter fileWriter = new FileWriter("webpage.txt");
            String content;
            while ((content = in.readLine()) != null)
                fileWriter.write(content + "\n");
            fileWriter.close();
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Client2 client11 = new Client2();
        client11.saveContentToFile();
    }
}
