import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Scanner;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class Server {
	private SSLServerSocket serverSocket;

    public Server() {
    	System.out.println("Server running");
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream tstore = Server.class
                    .getResourceAsStream("/servercert.pem");
            char[] pwd = {'c','h','a','n','g','e','i','t'};
            trustStore.load(tstore, pwd);
            //tstore.close();
            TrustManagerFactory tmf = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream kstore = Server.class
                    .getResourceAsStream("/servercert.pem");
            keyStore.load(kstore, pwd);
            KeyManagerFactory kmf = KeyManagerFactory
                    .getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, pwd);
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(),
                    SecureRandom.getInstanceStrong());

            SSLServerSocketFactory factory = ctx.getServerSocketFactory();
            try (ServerSocket listener = factory.createServerSocket(443)) {
                SSLServerSocket sslListener = (SSLServerSocket) listener;

                sslListener.setNeedClientAuth(false);
                sslListener.setEnabledProtocols(new String[]{"TLSv1.2"});
                while (true) {
                    try (Socket socket = sslListener.accept()) {
                        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                        File file = new File("webpage.txt");
                        Scanner scanner = new Scanner(file);
                        while (scanner.hasNextLine()) {
                            String data = scanner.nextLine();
                            out.println(data);
                        }
                        scanner.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
    }
}
