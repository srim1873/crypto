#unit tests

"""Assignment 2: Cryptography for CSUBB 2020.

Name: Tamás Viola Eszter
nr_matricol: tvim1879

"""

import unittest
import utils
from merkle_hellman_knapsack import (create_superincreasing_set, 
                                     generate_knapsack_key_pair,
                                     encrypt_mh, decrypt_mh)                                
from solitaire import (order_deck, encrypt_solitaire, decrypt_solitaire)
from testclient import testclient

class TestMerkleHellmann(unittest.TestCase):
    def test_superincreasing(self):
        supinc = create_superincreasing_set() 
        self.assertTrue(utils.is_superincreasing(supinc[0]))

    def test_one(self):
        key = generate_knapsack_key_pair() #private key, public key
        msg = 'Alma a fa alatt'
        enc = encrypt_mh(msg, key[1])
        dec = decrypt_mh(enc, key[0])

        self.assertEqual(msg, dec)

    def test_two(self):
        key = generate_knapsack_key_pair()
        msg = '1 AlmA a NAGY fa a1att'
        enc = encrypt_mh(msg, key[1])
        dec = decrypt_mh(enc, key[0])

        self.assertEqual(msg, dec)

    def test_three(self):
        key = generate_knapsack_key_pair()
        msg = '123456789'
        enc = encrypt_mh(msg, key[1])
        dec = decrypt_mh(enc, key[0])

        self.assertEqual(msg, dec)

class TestSolitaire(unittest.TestCase):
    def test_deck(self):
        deck = order_deck('PERSEPHONE')
        eq = [4, 5, 6, 7, 52, 'B', 10, 11, 12, 13, 14, 15, 16, 1, 19, 'A', 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 17, 32, 33, 34, 35, 36, 2, 39, 40, 41, 42, 43, 18, 9, 48, 49, 45, 31, 44, 37, 38, 8, 46, 47, 50, 51, 3, 30]
        self.assertEqual(deck, eq)

    def test_encdec(self):
        deck = order_deck('PASSPHRASE')
        msg = 'Do! not() eéncrypt'
        enctext = encrypt_solitaire(deck, msg)
        dectext = decrypt_solitaire(deck, enctext)
        self.assertEqual(dectext, 'DONOTENCRYPTXXX')

    def test_enc(self):
        deck = order_deck('almafa')
        msg = 'alma'
        enctext = encrypt_solitaire(deck, msg)
        self.assertEqual(enctext, 'HEXMZ')

    def test_dec(self):
        deck = order_deck('almafa')
        enc = 'HEXMZ'
        eq = 'ALMAX'
        dec = decrypt_solitaire(deck, enc)

        self.assertEqual(dec, eq)

class TestServer(unittest.TestCase):
    def test_registration_unsuccessful(self):
        msg = testclient('[2000 2001 2002 2003 2004]')
        self.assertEqual(msg, 'REGISTRATION/UPDATE UNSUCCESSFUL')

    def test_registration_successful(self):
        msg = testclient('2000 2001 2002 2003 2004 2001 2002 2003 2004')
        self.assertEqual(msg, 'PUBLIC KEY SUCCESFULLY REGISTERED/UPDATED')

    def test_get_key_ok(self):
        msg = testclient('2000 2001 2002 2003 2004 2001 2002 2003 2004')
        msg = testclient('2000')
        self.assertEqual(msg, '2001 2002 2003 2004 2001 2002 2003 2004')

    def test_get_key_not_ok(self):
        msg = testclient('2001')
        self.assertEqual(msg, '')






if __name__ == '__main__':
    unittest.main()