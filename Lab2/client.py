#implementing Peer to peer client


import socket
import threading
import time
from sys import argv
from merkle_hellman_knapsack import (generate_knapsack_key_pair,
                                     encrypt_mh, decrypt_mh)
from solitaire import (order_deck, encrypt_solitaire, decrypt_solitaire)


ENCODING = 'utf-8'
HOST = '127.0.0.1'
SERVERPORT = 8080

def define_port():
    port = 0
    if len(argv) < 2 :
        print("Usage: py", argv[0], "<port>\n")                  
    else: 
        try:
            port = int(argv[1])
        except ValueError:
            print("Port must be a number between 1024 and 49151")

    while port < 1024 or port > 49151:
        try:
            port = int(input("Please define a port:"))
        except ValueError:
            print("Port must be a number between 1024 and 49151")

    print("YOUR PORT IS", port)
    return port

def generate_and_register_key(port, sock):
    (private_key, public_key) = generate_knapsack_key_pair()

    msg = bytes(str(port), ENCODING) + b' '
    msg += bytes(' '.join(map(str, public_key)), ENCODING)
    print("REGISTERING KEY")
    # print("SENDING MESSAGE:", (str(msg, ENCODING)))
    sock.send(msg)
    _from_server = sock.recv(1024)
    # print("RECEIVED MESSAGE:", (str(from_server, ENCODING)))
    print("KEY REGISTERED")


    return private_key

def register_key_to_server(port):
    #connectiong to server
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((HOST, SERVERPORT))
    except socket.error as err:
        print("CONNECTION ERROR:" + str(err))
        exit() #or retry later

    #generating and registering own key
    private_key = generate_and_register_key(port, sock)

    sock.shutdown(2)
    sock.close()

    return (private_key)

def read_partner_id():
    id = 0
    while id < 1024 or id > 49151:
        try:
            id = int(input("Please define your partner's port:"))
        except ValueError:
            print("Port must be a number between 1024 and 49151")
    return id

def get_partner_key(sock, id):
    msg = bytes(str(id), ENCODING)
    # print("SENDING MESSAGE:", (str(msg, ENCODING)))
    sock.send(msg)
    from_server = sock.recv(1024)
    # print("RECEIVED MESSAGE:", (str(from_server, ENCODING)))

    server_msg = str(from_server, ENCODING).split(' ')
    try:
        partner_key = list(map(int, server_msg))
        if(partner_key[0] == 0):
            print("COULD NOT FIND CLIENT WITH THIS ID")
        return partner_key
    except ValueError:
        print("COULD NOT GET AN ANsWER")

def get_partner_key_from_server(partner_id):
    #connectiong to server
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((HOST, SERVERPORT))
    except socket.error as err:
        print("CONNECTION ERROR:" + str(err))
        exit() #or retry later
    
    #getting partner's public key
    partner_key = get_partner_key(sock, partner_id)

    # print("Partner's key successfully accuired :) ", partner_key)

    sock.shutdown(2)
    sock.close()

    return partner_key
       
def send_hello(host, port, myport, key):
    print('Sending hello')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))

    msg = 'HELLO ' + str(myport)
    enctext = encrypt_mh(msg, key)
    msg = bytes(' '.join(map(str, enctext)), ENCODING)

    s.send(msg)

    s.shutdown(2)
    s.close()
    print('Hello sent')

def wait_hello(host, port, key):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, port))
    sock.listen(1)
    print('Waiting for HELLO')
    conn, _ = sock.accept()
    try:
        data = conn.recv(1024)
        msg = list(map(int, str(data, ENCODING).split(' ')))
        dectext = decrypt_mh(msg, key)
        message = dectext.split(' ')
        if (message[0] == 'HELLO'):
            hello = int(message[1])
    finally:
        conn.shutdown(2)
        conn.close()

    print('HELLO received')
    return hello

def send_ack(host, port, myport, key):
    print('Sending ack')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))

    msg = 'ACK ' + str(myport)

    enctext = encrypt_mh(msg, key)
    msg = bytes(' '.join(map(str, enctext)), ENCODING)

    s.send(msg)

    s.shutdown(2)
    s.close()
    print('ACK sent')

def wait_ack(host, port, key):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, port))
    sock.listen(1)
    print('Waiting for ACK')
    conn, _ = sock.accept()
    try:
        data = conn.recv(1024)
        msg = list(map(int, str(data, ENCODING).split(' ')))
        dectext = decrypt_mh(msg, key)
        message = dectext.split(' ')
        if (message[0] == 'ACK'):
            ack = int(message[1])
    finally:
        conn.shutdown(2)
        conn.close()

    print('ACK received')
    return(ack)

def send_half_secret(host, port, key, secret):
    print('Sending half secret')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))

    msg = 'SECRET ' + str(secret)
    enctext = encrypt_mh(msg, key)
    msg = bytes(' '.join(map(str, enctext)), ENCODING)
    
    s.send(msg)

    s.shutdown(2)
    s.close()
    print('SECRET sent')

def wait_half_secret(host, port, key):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, port))
    sock.listen(1)
    print('Waiting for secret')
    conn, _ = sock.accept()
    try:
        data = conn.recv(1024)
        msg = list(map(int, str(data, ENCODING).split(' ')))
        dectext = decrypt_mh(msg, key)
        message = dectext.split(' ')
        if (message[0] == 'SECRET'):
            return message[1]
    finally:
        conn.shutdown(2)
        conn.close()
    print('SECRET received')    

def receiver(host, port, deck):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, port))
    sock.listen(1)
    print('Listening...')
    conn, _ = sock.accept()
    try:
        while True:
            data = conn.recv(1024)
            msg = data.decode(ENCODING)
            dectext = decrypt_solitaire(deck, msg)

            print("MESSAGE FROM CLIENT:", dectext)
            if dectext[0:4] == 'EXIT' or not data:
                print('Client shutting down')
                break
    finally:
        conn.shutdown(2)
        conn.close()
    print('Listening closed ...')

def sender(host, port, deck):
    print('Send ready ...') 
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))   
    while True:
        msg = input()
        enctext = encrypt_solitaire(deck, msg)
        s.send(enctext.encode(ENCODING))
        if msg.upper() == 'EXIT':
            break
    s.shutdown(2)
    s.close()
    print('Send closed ...')

def main():
    #defining client's own port
    myport = define_port()

    #generating and registering key
    mykey = register_key_to_server(myport)
    print('MYKEY', mykey)

    #lets user decide if he/she wants to initiate a connection
    # or would rather wait for someone else to connect him/her
    what_s_next = input('WOULD YOU LIKE TO SEND A MESSAGE?(Y/N)')
    if (what_s_next.upper() == 'Y'):
        #getting partner's id from console
        partner_id = read_partner_id()

        #getting partner's public key
        partner_key = get_partner_key_from_server(partner_id)

        #sending hello message
        send_hello(HOST, partner_id, myport, partner_key) 

        #wait for ack
        ack = wait_ack(HOST, myport, mykey)
        if (ack!=partner_id): 
            print('ACK never arrived, closing...')
            exit(0)

        #send half of phrase
        secret = input('Please define your part of the passphrase: ')
        send_half_secret(HOST, partner_id, partner_key, secret)

        #wait for response phrase     
        secret2 = wait_half_secret(HOST, myport, mykey)

        passph = secret + secret2

    else: 
        #waiting for a hello msg
        key = mykey
        partner_id = wait_hello(HOST, myport, key)

        #getting partner's public key
        partner_key = get_partner_key_from_server(partner_id)

        #sending back ack
        send_ack(HOST, partner_id, myport, partner_key)

        #wait for secret
        print('mykey is', mykey)
        print('partner key is', partner_key)
        secret = wait_half_secret(HOST, myport, mykey)

        #send own secret
        secret2 = input('Please define your part of the passphrase: ')
        send_half_secret(HOST, partner_id, partner_key, secret2)

        passph = secret + secret2

    print("passphrase is", passph)
    # #shuffle deck
    deck = order_deck(passph)

    #start listening on port 
    rcv = threading.Thread(target=receiver, args=(HOST, myport, deck))
    rcv.start()

    #start sending on port
    snd = threading.Thread(target=sender, args=(HOST, partner_id, deck))
    snd.start()

main()