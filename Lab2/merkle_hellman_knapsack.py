#implementing the Merkle-Hellmann Knapsack Cryptosystem


import utils
import random

SIZE = 8

def create_superincreasing_set():
    r = random.randint(2, 10)
    w = [r]
    total = r
    for _ in range(0, SIZE-1):
        wi = random.randint(total+1, 2*total)
        w.append(wi)
        total += wi

    q = random.randint(total+1, 2*total)
    return w, q

def find_coprime(q):
    r = random.randint(2, q-1)
    while not utils.coprime(q, r):
        r = random.randint(2, q-1)
    return r

def generate_private_key():
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

        Following the instructions in the handout, construct the private key
        components of the MH Cryptosystem. This consists of 3 tasks:

        1. Build a superincreasing sequence `w` of length n
            Note: You can double-check that a sequence is superincreasing by using:
                `utils.is_superincreasing(seq)`
        2. Choose some integer `q` greater than the sum of all elements in `w`
        3. Discover an integer `r` between 2 and q that is coprime to `q`
            Note: You can use `utils.coprime(r, q)` for this.

        You'll also need to use the random module's `randint` function, which you
        will have to import.

        Somehow, you'll have to return all three of these values from this function!
        Can we do that in Python?!

        :param n: Bitsize of message to send (defaults to 8)
        :type n: int

        :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
        """
    (w, q) = create_superincreasing_set()
    r = find_coprime(q)
    return (w, q, r)

def create_public_key(w, q, r):
    """Create a public key corresponding to the given private key.

        To accomplish this, you only need to build and return `beta` as described in
        the handout.

            beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

        Hint: this can be written in one or two lines using list comprehensions.

        :param private_key: The private key created by generate_private_key.
        :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

        :returns: n-tuple public key
        """

    beta = []
    for i in range(0, len(w)):
        beta.append(r*w[i] % q)
    return beta

def encrypt_mh(msg, b):
    """Encrypt an outgoing message using a public key.

        Following the outline of the handout, you will need to:
        1. Separate the message into chunks based on the size of the public key.
            In our case, that's the fixed value n = 8, corresponding to a single
            byte. In principle, we should work for any value of n, but we'll
            assert that it's fine to operate byte-by-byte.
        2. For each byte, determine its 8 bits (the `a_i`s). You can use
            `utils.byte_to_bits(byte)`.
        3. Encrypt the 8 message bits by computing
             c = sum of a_i * b_i for i = 1 to n
        4. Return a list of the encrypted ciphertexts for each chunk of the message.

        Hint: Think about using `zip` and other tools we've discussed in class.

        :param message: The message to be encrypted.
        :type message: bytes
        :param public_key: The public key of the message's recipient.
        :type public_key: n-tuple of ints

        :returns: Encrypted message bytes represented as a list of ints.
        """

    encdata = []
    for c in msg[0:len(msg)]:
        alpha = utils.byte_to_bits(ord(c))
        # print("alpha:", alpha)
        s = 0
        for i in range(0, len(alpha)):
            # print("alpha * b: ", alpha[i], b[i])
            s += alpha[i] * b[i]
        # print(s)
        encdata.append(s)
    return encdata

def calculate_modinv(q, r):
    return utils.modinv(r, q)

def solve_subset_sum(c, w):
    # print("subset sum is", c)
    alpha = [0] * SIZE
    for i in range(SIZE-1, -1, -1):
        if w[i] <= c:
            alpha[i] = 1
            c -= w[i]
    # print(c)
    return alpha

def decrypt_mh(msg, private_key):
    """Decrypt an incoming message using a private key.

        Following the outline of the handout, you will need to:
        1. Extract w, q, and r from the private key.
        2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
            algorithm (implemented for you at `utils.modinv(r, q)`)
        3. For each byte-sized chunk, compute
             c' = cs (mod q)
        4. Solve the superincreasing subset sum problem using c' and w to recover
            the original plaintext byte.
        5. Reconstitute the decrypted bytes to form the original message.

        :param message: Encrypted message chunks.
        :type message: list of ints
        :param private_key: The private key of the recipient (you).
        :type private_key: 3-tuple of w, q, and r

        :returns: bytearray or str of decrypted characters
        """
    decdata = ""
    r = private_key[-1]
    # print(r)
    q = private_key[-2]
    # print(q)
    w = private_key[0:-2]
    # print(w)

    s = calculate_modinv(q, r)
    # print("s:", s)

    for c in msg[0:len(msg)]:
        cc = c*s%q
        alpha = solve_subset_sum(cc, w)
        # print(alpha)
        i = utils.bits_to_byte(alpha)
        # print(i)
        decdata += chr(i)

    return decdata

def generate_knapsack_key_pair():
    (w, q, r) = generate_private_key()
    public_key = create_public_key(w, q, r)

    private_key = w
    private_key.append(q)
    private_key.append(r)

    return(private_key, public_key)

def merkle_hellman(msg):
    #calculating private key
    (w, q, r) = generate_private_key()
    # print("W:", w)
    # print("q:", q)
    # print ("r:", r)

    #calculating public key
    b = create_public_key(w, q, r)
    # print("B:", b)

    #ENCRYPTING
    c = encrypt_mh(msg, b)
    print(c)
     
    #DECRYPTING
    msg = c
    private_key = w
    private_key.append(q)
    private_key.append(r)
    # print(private_key)
    dec = decrypt_mh(msg, private_key)    
    print(dec)
