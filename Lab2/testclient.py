#Client for testing

"""Assignment 2: Cryptography for CSUBB 2020.

Name: Tamás Viola Eszter
nr_matricol: tvim1879

"""
import socket
from sys import argv

IP = '127.0.0.1'
SERVERPORT = 8080

ENCODING = 'utf-8'


def testclient(msg):
    #connecting to server
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client_socket.connect((IP, SERVERPORT))
    except socket.error as err:
        print("CONNECTION ERROR:" + str(err))
        exit()
    
    msg = bytes(str(msg), ENCODING)
    client_socket.send(msg)

    from_server = client_socket.recv(1024)

    # shut down socket's connection tp server 
    client_socket.send(b'EXITING SERVER')
    client_socket.shutdown(2)
    client_socket.close()

    return str(from_server, ENCODING)


