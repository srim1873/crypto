#implementing the Solitaire Cipher

N = 54 #size of the deck
PASSPHRASE = "COTYLEDON" # after Cotyledon Tomentosa, alas Bear-paw succulent :)
#80 chars passphrase is highly recommended :P
ABC = " ABCDEFGHIJKLMNOPQRSTUVWXYZ"

# 2 decks: 54 cards, both JOKER is worth 53!

def swap(deck, x, y):
    if (y == 0):
        deck = deck[0:1] + deck[x:x+1] + deck[1:x] + deck[x+1:N]
    else:
        deck = deck[0:x] + deck[x+1:y+1] + deck[x:x+1] + deck[y+1:N] 
    return deck

def triple_cut(deck, a, b):
    if (a < b):
        first_joker = a
        last_joker = b
    else:
        first_joker = b
        last_joker = a

    first_part = deck[0:first_joker]
    middle_part = deck[first_joker:last_joker+1]
    last_part = deck[last_joker+1:54]

    deck = last_part + middle_part + first_part

    return deck

def count_cut(deck):
    count = deck[N-1]
    if (count == 'A' or count == 'B'):
        count = 53

    before_cut = deck[0:count]
    after_cut = deck[count:N-1]
    
    newdeck = after_cut + before_cut
    newdeck.append(deck[N-1])
    
    return newdeck

def get_output_card(deck):
    topcard = deck[0]
    if (topcard == 'A' or topcard == 'B'):
        topcard = 53
    
    if (deck[topcard] != 'A' and deck[topcard] != 'B'):
        return(deck[topcard])   # topcard and not topcard +1 since first card has index 0.
    else:
        return -1  

def generate_keystream(deck):
    joker_A_pos = deck.index('A')
    joker_B_pos = deck.index('B')

    #first step: move A one card down: 
    deck = swap(deck, joker_A_pos, (joker_A_pos+1)%N)
    joker_A_pos = deck.index('A')

    if (joker_A_pos == joker_B_pos): #if the two jokers were switched
        joker_B_pos = deck.index('B')
    
    # print(deck)

    #second step: move B two cards down:
    deck = swap(deck, joker_B_pos, (joker_B_pos+2)%N)
    joker_B_pos = deck.index('B')
    if (joker_A_pos == joker_B_pos or joker_A_pos == joker_B_pos-1): #if the joker was shifted
        joker_A_pos = deck.index('A')
    
    # print(deck) 
    
    #third step: triple cut:
    deck = triple_cut(deck, joker_A_pos, joker_B_pos)
    joker_A_pos = deck.index('A')
    joker_B_pos = deck.index('B')

    # print(deck)

    #fourth step: count cut
    deck = count_cut(deck)
    
    # print(deck)

    #fifth step: get key value
    key = get_output_card(deck)

    if (key == -1):
        # print(key)
        (deck, key) = generate_keystream(deck)
    # print(key)
    # print(deck)

    return (deck, key)

def count_cut_by_value(deck, cut):
    count = cut
    
    before_cut = deck[0:count]
    after_cut = deck[count:N-1]
    
    newdeck = after_cut + before_cut
    newdeck.append(deck[N-1])
    
    return newdeck

def generate_initial_ordering(deck, cut):
    joker_A_pos = deck.index('A')
    joker_B_pos = deck.index('B')

    #first step: move A one card down: 
    deck = swap(deck, joker_A_pos, (joker_A_pos+1)%N)
    joker_A_pos = deck.index('A')

    if (joker_A_pos == joker_B_pos): #if the two jokers were switched
        joker_B_pos = deck.index('B')
    
    # print(deck)

    #second step: move B two cards down:
    deck = swap(deck, joker_B_pos, (joker_B_pos+2)%N)
    joker_B_pos = deck.index('B')
    if (joker_A_pos == joker_B_pos or joker_A_pos == joker_B_pos-1): #if the joker was shifted
        joker_A_pos = deck.index('A')
    
    # print(deck) 
    
    #third step: triple cut:
    deck = triple_cut(deck, joker_A_pos, joker_B_pos)
    joker_A_pos = deck.index('A')
    joker_B_pos = deck.index('B')

    # print(deck)

    #fourth step: count cut
    deck = count_cut_by_value(deck, cut)

    return deck

def start_generating_keystream(deck, len):
    # print(deck)

    keystream = []
    for _ in range (0, len):
        (deck, key) = generate_keystream(deck)
        # print(key)
        keystream.append(key)

    return keystream

def simplify(msg): #filters whitespaces, non-abc chars, like numbers and punctuation marks as well
    simpletext = ""
    for c in msg:
        c = c.upper()
        if (c.isalpha() and ABC.find(c) != -1):
            simpletext += c
    # print(simpletext)
    return simpletext

def insert_joker(deck, pos, newpos):
    joker = deck[pos:pos+1]
    deck = deck[0:pos] + deck[pos+1:N]
    return deck[0:newpos] + joker + deck[newpos:N] 

def order_deck(passphrase):
     #initialisig the deck
    deck = list(range(1, N+1)) #card nr. 53, 54 are Jokers A and B (their walue is both 53)
    joker_A_pos = N-2
    joker_B_pos = N-1
    deck[joker_A_pos] = 'A'
    deck[joker_B_pos] = 'B'

    passphrase = simplify(passphrase)

    for c in passphrase[0:-2]:
        deck = generate_initial_ordering(deck, ABC.index(c))

    deck = insert_joker(deck, deck.index('A'), ABC.index(passphrase[-2]))
    deck = insert_joker(deck, deck.index('B'), ABC.index(passphrase[-1]))
    # print(deck)

    return(deck)

def encrypt_solitaire(deck, msg):
    msg = simplify(msg)
    msglen = len(msg)
    fives = msglen//5
    print(fives)
    print(msglen//5)
    print(msglen/5)
    if msglen//5 < msglen/5:
        fives += 1

    keystream = start_generating_keystream(deck, 5 * fives)
    print(keystream)
    print(fives)
    enctext = ""
    for i in range(0, msglen):
        enctext += ABC[((keystream[i] + ABC.index(msg[i])) %26) +1] # +1, bc 0.th index is empty

        if ((i+1)%5 == 0):
            enctext +=' '

    for i in range(0, 5*fives - msglen):
        enctext += ABC[((keystream[msglen+i] + ABC.index('X')) %26) +1]

    return enctext

def decrypt_solitaire(deck, msg):
    msg = simplify(msg)
    msglen = len(msg)

    keystream = start_generating_keystream(deck, msglen)

    dectext = ""
    for i in range(0, msglen):
        dectext += ABC[(ABC.index(msg[i]) - keystream[i] -2) %26 +1] 
        # -2 is needed bc i made that trick w creating ABC so 'A' is represented by index 1 and not the 0th
        # thought it'll will be a smart ideea, then caused some havoc, but i'm too lazy to rewrite it now, sorry

    return dectext

deck = order_deck('PASSPHRASE')
msg = 'Do! not() esncrypt'
enctext = encrypt_solitaire(deck, msg)
dectext = decrypt_solitaire(deck, enctext)