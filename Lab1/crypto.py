"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this placeholder text with a description of this module.
"""
import utils

#################
# CAESAR CIPHER #
#################
import string


def encrypt_caesar(plaintext):
    """Encrypt a plaintext using a Caesar cipher.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """

    # Your implementation here.

    characters = string.ascii_uppercase
    lowercase = string.ascii_lowercase

    result = ""

    for char in plaintext:
        if char in lowercase:
            return "Warning: Can't use lowercase characters"
        elif char not in characters:
            result += char
        else:
            index = characters.find(char)
            result += characters[(index + 3) % len(characters)]

    return result
    # raise NotImplementedError('encrypt_caesar is not yet implemented!')


def encrypt_caesar_with_shift(plaintext, shift):
    """Encrypt a plaintext using a Caesar cipher.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """

    # Your implementation here.

    characters = string.ascii_uppercase
    lowercase = string.ascii_lowercase

    result = ""

    for char in plaintext:
        if char in lowercase:
            return "Warning: Can't use lowercase characters"
        elif char not in characters:
            result += char
        else:
            index = characters.find(char)
            result += characters[(index + shift) % len(characters)]

    return result


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    characters = string.ascii_uppercase
    lowercase = string.ascii_lowercase

    result = ""

    for char in ciphertext:
        if char in lowercase:
            return "Warning: Can't use lowercase characters"
        elif char not in characters:
            result += char
        else:
            index = characters.find(char)
            result += characters[(index - 3) % len(characters)]

    return result
    # raise NotImplementedError('decrypt_caesar is not yet implemented!')


def decrypt_caesar_with_shift(ciphertext, shift):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    characters = string.ascii_uppercase
    lowercase = string.ascii_lowercase

    result = ""

    for char in ciphertext:
        if char in lowercase:
            return "Warning: Can't use lowercase characters"
        elif char not in characters:
            result += char
        else:
            index = characters.find(char)
            result += characters[(index - shift) % len(characters)]

    return result


###################
# VIGENERE CIPHER #
###################
def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The encrypted ciphertext.
    """
    characters = string.ascii_uppercase
    result = ""

    for i in range(len(plaintext)):
        if plaintext[i] not in characters:
            result += plaintext[i]
        else:
            index = characters.find(plaintext[i]) + characters.find(keyword[i % len(keyword)])
            result += characters[index % len(characters)]

    return result

    # Your implementation here.
    # raise NotImplementedError('encrypt_vigenere is not yet implemented!')


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    characters = string.ascii_uppercase
    result = ""
    j = 0

    for i in range(len(ciphertext)):
        if ciphertext[i] not in characters:
            result += ciphertext[i]
        else:
            index = characters.find(ciphertext[i]) - characters.find(keyword[j % len(keyword)])
            j += 1
            result += characters[index % len(characters)]

    return result
    # raise NotImplementedError('decrypt_vigenere is not yet implemented!')



########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################


def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    # Your implementation here.
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')

###################
# SCYTALE CIPHER #
###################

def encrypt_scytale(plaintext, circumference):
    length = len(plaintext) / circumference + 1
    result = ""
    for row in range(circumference):
        for index in range(length):  # oszlop
            if row + index * circumference < len(plaintext):
                result += plaintext[row + index * circumference]

    #print(result)
    return result


def decrypt_scytale(ciphertext, circumference):
    length = len(ciphertext) / circumference
    result = ""
    dif = len(ciphertext) - length * circumference

    for col in range(length):
        prev = col
        for row in range(circumference):
            if 0 < row <= dif:
                prev += 1
            result += ciphertext[prev]
            add = length
            prev = prev + add

    add = 0
    while dif > 0:
        #print(length + add, ciphertext[length + add])
        result += ciphertext[length + add]
        add += length + 1
        dif -= 1

    #print(result)
    return result


###################
# RAILFENCE CIPHER #
###################

def get_index(row, i, dec, asc, previous):
    if i == 0:
        return row
    elif dec == 0:
        return previous + asc
    elif asc == 0:
        return previous + dec
    elif i % 2 == 0:
        return previous + asc
    else:
        return previous + dec


def encrypt_railfence(plaintext, num_rails):
    dec = (num_rails - 1) * 2
    asc = 0
    result = ""

    for row in range(num_rails):
        i = 0
        previous = 0
        while previous < len(plaintext):
            index = get_index(row, i, dec, asc, previous)
            if index < len(plaintext):
                result += plaintext[index]
            previous = index
            i += 1
        dec -= 2
        asc += 2

    return result


def decrypt_railfence(ciphertext, num_rails):
    dec = (num_rails - 1) * 2
    asc = 0
    arr = ['0'] * len(ciphertext)
    j = 0

    for row in range(num_rails):
        i = 0
        previous = 0
        while previous < len(ciphertext):
            index = get_index(row, i, dec, asc, previous)
            if index < len(ciphertext):
                arr[index] = ciphertext[j]
                j += 1
            previous = index
            i += 1
        dec -= 2
        asc += 2

    result = ''.join(arr)

    return result

