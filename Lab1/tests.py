from crypto import (encrypt_caesar, decrypt_caesar,
                    encrypt_vigenere, decrypt_vigenere,
                    generate_private_key, create_public_key,
                    encrypt_mh, decrypt_mh)


def test_caesar():
    with open('tests/caesar-tests.txt') as f:
        for line in f:
            line = line.replace("\n", "")
            str1, str2 = line.split("\t")
            if encrypt_caesar(str1) != str2:
                print ("WARNING(encrypt): " + encrypt_caesar(str1) + " != " + str2)
            if str1 != decrypt_caesar(str2):
                print ("WARNING(decrypt): " + decrypt_caesar(str2) + " != " + str1)

    print("Test caesar done!")


def test_vigenere():
    with open('tests/vigenere-tests.txt') as f:
        for line in f:
            line = line.replace("\n", "")
            plaintext, keyword, ciphertext = line.split("\t")
            if encrypt_vigenere(plaintext, keyword) != ciphertext:
                print ("WARNING(encrypt): " + encrypt_vigenere(plaintext, keyword) + " != " + ciphertext)
            if plaintext != decrypt_vigenere(ciphertext, keyword):
                print ("WARNING(decrypt): " + decrypt_vigenere(ciphertext, keyword) + " != " + plaintext)

    print ("Test vigenere done!")


test_caesar()
test_vigenere()
